# 文档地址
https://www.bookstack.cn/read/gin-EDDYCJY-blog/golang-gin-2018-02-16-Gin%E5%AE%9E%E8%B7%B5-%E8%BF%9E%E8%BD%BD%E4%B8%80-Golang%E4%BB%8B%E7%BB%8D%E4%B8%8E%E7%8E%AF%E5%A2%83%E5%AE%89%E8%A3%85.md

# 目录结构

```shell
D:.
├─conf          //用于存储配置文件
├─middleware   //应用中间件
├─models      //应用数据库模型
├─pkg         // 第三方包
├─routers     //路由逻辑处理
└─runtime     // 应用运行时数据
```